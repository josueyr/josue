<html>
<head>
<meta charset="UTF-8"/>
<?php
	include('cesarphp.php');
?>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<h1>
	Método del cifrado del César
</h1>
<hr/>
<?php header("Content-Type: text/html; charset=utf-8");
	if (!isset($_GET["pagina"])){
		echo "<div class='container marketing'>";
        echo "<div class='row'>";
        echo "<div class='col-md-4' name='principal'>";
        echo  "<div class='panel panel-info'>";
        echo "<div class='panel-heading' style='text-align:center; font-size:1.3em'>SELECCIONA UNA OPCION</div>";
        echo "<div class='panel-body'>";
        echo "<form action='cesar.php' method='get'>";
		echo "<input type='submit' class='btn btn-lg btn-primary' value='Ir a cifrar Texto'>";
		echo "<input type='hidden' name='pagina' value='cifrar'>";
		echo "</form>";
		echo "<form action='cesar.php' method='get'>";
		echo "<input type='submit' class='btn btn-lg btn-primary' value ='Ir a descifrar texto'>";
		echo "<input type='hidden' name='pagina' value='descifrar'>";
		echo "<form>";
        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</div>";
	}
	else{
		$mipag = $_GET["pagina"];
		if ($mipag == 'cifrar'){
			echo "<div class='container marketing'>";
	        echo "<div class='row'>";
	        echo "<div class='col-md-4' name='cifrar'>";
	        echo  "<div class='panel panel-warning'>";
	        echo "<div class='panel-heading' style='text-align:center; font-size:1.3em'>HAS SELECCIONADO CIFRAR</div>";
	        echo "<div class='panel-body'>";
			echo "<form action='cesar.php' method='get'>";
			echo "<div class='form-group'>";
            echo "<label for='textOrigin'>Texto Original</label>";
            echo "<input type='text' id='textOrigin' name ='original' class='form-control' placeholder='Texto original'>";
            echo "</div>";
            echo "<div class='form-group'>";
            echo "<label for='textDes'>Desplazamiento</label>";
            echo "<input type='text' id='textDes' name='desplazamiento' class='form-control' placeholder='Desplazamiento'>";
            echo "</div>";
			echo "<input type='submit' class='btn btn-lg btn-warning' value='Cifrar Texto'>";
			echo "<input type='hidden' name='pagina' value='cifrarTexto'>";
			echo "</form>";
			echo "<div class='panel-footer' style='text-align:right'><a href='cesar.php'>Volver</a></div>";
			echo "<div/>";
			echo "</div>";
	        echo "</div>";
	        echo "</div>";

		}
		else if ($mipag == 'descifrar'){
			echo "<div class='container marketing'>";
	        echo "<div class='row'>";
	        echo "<div class='col-md-4' name='descifrar'>";
	        echo  "<div class='panel panel-success'>";
	        echo "<div class='panel-heading' style='text-align:center; font-size:1.3em'>HAS SELECCIONADO DESCIFRAR</div>";
	        echo "<div class='panel-body'>";
			echo "<form action='cesar.php' method='get'>";
			echo "<div class='form-group'>";
            echo "<label for='textCif'>Texto Cifrado</label>";
            echo "<input type='text' id='textCif' name ='cifrado' class='form-control' placeholder='Texto cifrado'>";
            echo "</div>";
            echo "<div class='form-group'>";
            echo "<label for='textDesCi'>Desplazamiento</label>";
            echo "<input type='text' id='textDesCi' name='desplazamientoInverso' class='form-control' placeholder='Desplazamiento'>";
            echo "</div>";
			echo "<input type='submit' class='btn btn-lg btn-success' value='Descifrar Texto'>";
			echo "<input type='hidden' name='pagina' value='descifrarTexto'>";
			echo "</form>";
			echo "<div class='panel-footer' style='text-align:right'><a href='cesar.php'>Volver</a></div>";
			echo "<div/>";
			echo "</div>";
	        echo "</div>";
	        echo "</div>";
	    
		}
		else if ($mipag == 'descifrarTexto'){
			$cadena = $_GET['cifrado'];
			$desplazamiento = $_GET['desplazamientoInverso'];
			$resultado = descifrar($cadena, $desplazamiento);

			echo "<div class='container marketing'>";
	        echo "<div class='row'>";
	        echo "<div class='col-md-4' name='descifrar'>";
	        echo  "<div class='panel panel-success'>";
	        echo "<div class='panel-heading' style='text-align:center; font-size:1.3em'>RESULTADO DE DESCIFRAR</div>";
	        echo "<div class='panel-body'>";
			echo "<form action='cesar.php' method='get'>";
			echo "<div class='form-group'>";
            echo "<label for='textCif'>TEXTO ORIGINAL</label>";
            echo "<input type='text' id='textCif' name ='cifrado' class='form-control' placeholder='".$cadena."'>";
            echo "</div>";
            echo "<div class='form-group'>";
            echo "<label for='textDesCi'>TEXTO DESCIFRADO</label>";
            echo "<input type='text' id='textDesCi' name='desplazamientoInverso' class='form-control' placeholder='".$resultado."' disabled>";
            echo "</div>";
			echo "<div style='text-align:right'><a href='cesar.php'>Volver</a></div>";
			echo "</form>";
			echo "<div/>";
			echo "</div>";
	        echo "</div>";
	        echo "</div>";
		}
		else if ($mipag == 'cifrarTexto'){
			$cadena = $_GET['original'];
			$desplazamiento = $_GET['desplazamiento'];
			$resultado = cifrar($cadena, $desplazamiento);
			echo "<div class='container marketing'>";
	        echo "<div class='row'>";
	        echo "<div class='col-md-4' name='descifrar'>";
	        echo  "<div class='panel panel-warning'>";
	        echo "<div class='panel-heading' style='text-align:center; font-size:1.3em'>RESULTADO DE CIFRAR</div>";
	        echo "<div class='panel-body'>";
			echo "<form action='cesar.php' method='get'>";
			echo "<div class='form-group'>";
            echo "<label for='textO'>TEXTO ORIGINAL</label>";
            echo "<input type='text' id='textO' name ='cifrado' class='form-control' placeholder='".$cadena."'>";
            echo "</div>";
            echo "<div class='form-group'>";
            echo "<label for='textCi'>TEXTO CIFRADO</label>";
            echo "<input type='text'id='textCi' name='desplazamientoInverso' class='form-control' placeholder='".$resultado."' disabled>";
            echo "</div>";
			echo "<div style='text-align:right'><a href='cesar.php'>Volver</a></div>";
			echo "</form>";
			echo "<div/>";
			echo "</div>";
	        echo "</div>";
	        echo "</div>";
		}
	}
?>

</div>

</body>
</html>