<?php
$arrayLetras = array('a' => 0, 'b' => 1, 'c' => 2, 'd' => 3, 'e' => 4, 'f' => 5, 'g' => 6, 'h' => 7, 'i' => 8, 'j' => 9, 'k' => 10, 'l' => 11, 'm' => 12, 'n' => 13,
'ñ' => 14, 'o' => 15, 'p' => 16, 'q' => 17, 'r' => 18, 's' => 19, 't' => 20, 'u' => 21, 'v' => 22, 'w' => 23, 'x' => 24, 'y' => 25, 'z' => 26);
$arrayNumeros = array_flip($arrayLetras);
$arrayCaracteres = array(" ", ',', ".", "-", "_");
$cadenaFinal = "";

function cifrar($cad, $desplazamiento){
	global $arrayLetras, $arrayNumeros, $cadenaFinal;
	$cadena = strtolower( $cad );

	for ($i =0; $i<strlen($cadena);$i++){
		if (esCaracterRaro($cadena[$i]))
			$cadenaFinal = $cadenaFinal.$cadena[$i];
		else{
			$posicion = $arrayLetras[$cadena[$i]];
			$resultado = ($posicion + $desplazamiento) % 27;
			$cadenaFinal = $cadenaFinal.$arrayNumeros[$resultado];
		}
	}
	return $cadenaFinal;
}
function esCaracterRaro($caracter){
	global $arrayCaracteres;
	$elementos = count($arrayCaracteres);
	$raro = false;
	for($i=0; $i<$elementos; $i++){
		if ($arrayCaracteres[$i] == $caracter)
			$raro = true;
	}
	if ($raro)
		return true;
	else
		return false; 
}
function descifrar($cad, $desplazamiento){
	global $arrayLetras, $arrayNumeros, $cadenaFinal;
	$cadena = strtolower( $cad );

	for ($i =0; $i<strlen($cadena);$i++){
		if (esCaracterRaro($cadena[$i]))
			$cadenaFinal = $cadenaFinal.$cadena[$i];
		else{
			$posicion = $arrayLetras[$cadena[$i]];
			$resultado = (($posicion - $desplazamiento)+27) % 27;
			$cadenaFinal = $cadenaFinal.$arrayNumeros[$resultado];
		}
	}
	return $cadenaFinal;
}
?>