<html>
<head>
<meta content="es">
<meta charset="Utf-8">
<?php
include_once('funciones.php');
session_start();
?>
</head>
<body>
<table width='100%'>
<tr>
	<td> <?php include_once('cabecera.php'); ?> </td>
</tr>
<tr align="center">
<td>
<?php
	$advertencia = "";
	$advertenciaPass = "";
	$pass = md5(md5("hola"));
	if (isset($_POST["usuario"])){
		$usuario = $_POST["usuario"];
		$passUsuario = md5(md5($_POST["pass"]));
		if (strlen($usuario) == 0 || strlen($passUsuario) == 0)
			$advertencia = "Inserte usuario y contraseña para loguear.";
		else{
			if ($passUsuario != $pass)
				$advertenciaPass = "Contraseña incorrecta.";
			else{
				$_SESSION["user"] = $usuario;
				header("Location: login.php");
				return;
			}
		}
	}
	if(isset($_SESSION["user"])){
		header("Location: index1.php");
		return;
	}
?>
		<form method="post">
		<?php echo "<span style='color: red'>$advertencia</span><br/>"; ?>
		<label for="usuario" style="color: blue">Usuario:</label>
		<input type="text" name="usuario" id="usuario"/>
		<?php
		if (isset($_COOKIE["galleta"]))
		 	echo "<span style='color: red'>Cookie: Usuario ->".$_COOKIE["galleta"]."</span>";
		 ?>
		<br/>
		<?php echo "<span style='color: red'>$advertenciaPass</span><br/>"; ?>
		<label for="pass" style="color: blue">Contraseña:</label>
		<input type="password" name="pass" id="pass"/>
		<?php
		if (isset($_SESSION["tiempoExpirado"])){
		 	echo "<span style='color: red'>".$_SESSION["tiempoExpirado"]."</span><br/>"; 
		 	unset($_SESSION["tiempoExpirado"]);
		 }
		 ?>
		<br/>
		<input type="submit" value="Log in"/>
		</form>

</td>
</tr>
<tr>
	<td> <?php include_once('pie.php'); ?> </td>
</tr>
</table>
</body>
</html>
