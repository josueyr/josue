<?php
session_start();
if (!isset($_SESSION["user"])){ // si no existe usuario, reedireccionamos al login
	header("Location: login.php");
	return;
}
else{
	setcookie("galleta", $_SESSION["user"]); // creamos una cookie con su usuario
	if (!isset($_SESSION["minuto"]))
		 $_SESSION["minuto"] = date('Y-n-j H:i:s'); // creamos una sesion que controle el tiempo de espera
	else{
		$actual = date('Y-n-j H:i:s');
		$diferencia = (strtotime($actual)-strtotime($_SESSION["minuto"]));
		if ($diferencia>=60){
			header("Location: logout.php"); // si es mayor de un minuto, reedireccionar al login
			return;
		}
		else
			$_SESSION["minuto"] = date('Y-n-j H:i:s');
	}		
}

?>
<html>
<head>
<meta content="es">
<meta charset="Utf-8">
<?php
include_once('funciones.php');
?>
<style type="text/css">
h2{
	color:blue;
	text-align: center;
}
</style>
</head>
<body>
<table width='100%'>
<tr>
	<td> <?php include('cabecera.php'); ?> </td>
</tr>

<?php
if (isset($_SESSION["aGuardar"])){ //guardar datos en la agenda
	guardarDatosFichero("txt/agenda.txt", $_SESSION["aGuardar"]);
			echo "<tr align='center'>";
			echo "<td>";
			echo "<h2> Añadir </h2>";
			echo "<h3 style='color:green'> Datos guardados con éxito </h3>";
			echo "<hr/>";
			echo "</td>";
			echo "</tr>";
			unset($_SESSION["aGuardar"]);
}
elseif(isset($_SESSION["aBorrar"])){ //borrar datos en la agenda
	$datos = leerDatosFichero("txt/agenda.txt");
	eliminarDatos($datos,$_SESSION["aBorrar"]);	
			echo "<tr align='center'>";
			echo "<td>";
			echo "<h2> Borrar </h2>";
			echo "</td></tr>";
			echo "<tr align='center'><td>ID: ".$_SESSION["aBorrar"]."BORRADO </td></tr>";
			echo "<tr align='center'><td>Datos modificados con éxito<hr/></td></tr>";
			unset($_SESSION["aBorrar"]);
}
elseif (isset($_POST["Inicio"])){ //pantalla principal
	echo "<tr align='center'>";
	echo "<td>";
		echo "<h2> Bienvenido ".$_SESSION["user"]."</h2>";
		echo "<h2> Seleccione una opción </h2>";
	echo "</td>";
echo "</tr>";
}
elseif (isset($_POST["pagina"]) && $_POST["pagina"] == 'Salir'){
		header("Location: logout.php");
		return;
}
elseif (isset($_POST["pagina"]) && $_POST["pagina"] == 'Listar'){ //lista de los contactos en la agenda
		echo "<tr align='center'>";
		echo "<td>";
		echo "<h2> Listar </h2>";
		echo "<table border=1px>";
		echo "<tr><td>ID</td><td>USUARIO</td><td>TELEFONO</td><td>EMAIL</td></tr>";
		$datos = leerDatosFichero("txt/agenda.txt");
		for($i = 0; $i< count($datos);$i++){
			$array = explode(",", $datos[$i]);
			echo "<tr><td>$i</td><td>".$array[0]."</td><td>".$array[1]."</td><td>".$array[2]."</td></tr>";
		}
		echo "</table>";
		echo "<hr/>";
		echo "</td>";
		echo "</tr>";
}
elseif (isset($_POST["pagina"]) && $_POST["pagina"] == 'Añadir'){ //página para añadir contactos
		echo "<tr align='center'>";
		echo "<td>";
		echo "<h2> Añadir </h2>";
		echo "<h3> Añadir datos </h3>";
		echo "<form method='post'>";
		echo "USUARIO: <input type='text' name='user' value=''/> <br/>";
		echo "TELEFONO: <input type='text' name='tf' value=''/> <br/>";
		echo "EMAIL: <input type='text' name='email' value=''/> <br/>";
		echo "<input type='submit' value='Añadir'/>";
		echo "<input type='hidden' name='pagina' value='add'/>";
		echo "<input type='reset' value='Cancelar'/>";
		echo "</form>";
		echo "<hr/>";
		echo "</td>";
		echo "</tr>";
}
elseif (isset($_POST["pagina"]) && $_POST["pagina"] == 'Borrar'){ //pagina para borrar contactos
		echo "<tr align='center'>";
		echo "<td>";
			echo "<h2> Borrar </h2>";
			echo "<table border=1px>";
		echo "<tr><td>ID</td><td>USUARIO</td><td>TELEFONO</td><td>EMAIL</td></tr>";
		$datos = leerDatosFichero("txt/agenda.txt");
		for($i = 0; $i< count($datos);$i++){
			$array = explode(",", $datos[$i]);
			echo "<tr><td>$i</td><td>".$array[0]."</td><td>".$array[1]."</td><td>".$array[2]."</td></tr>";
		}
		echo "</table>";
		echo "</td>";
		echo "</tr>";
		echo "<tr align='center'><td><form method='post'> Introduce el ID del elemento a eliminar: <input type='text' name='idEliminar' value=''/></td>";
		echo "<tr align='center'><td><input type='submit' value='Borrar'/>";
		echo "<input type='hidden' name='pagina' value='delete'/>";
		echo "<input type='reset' value='Cancelar'/><hr/></form>";
		echo "</td></tr>";
}
elseif (isset($_POST["pagina"]) && $_POST["pagina"] == 'add'){
		if((strlen($_POST["email"]) == 0 || strlen($_POST["user"]) == 0) || strlen($_POST["tf"]) == 0 ){ // si los campos a añadir estan vacios
			echo "<tr align='center'>";
			echo "<td>";
			echo "<h2> Añadir datos </h2>";
			echo "<span style='color:red'> Rellene todo los campos para añadir contacto </span><br/>";
			echo "<form method='post'>";
			echo "USUARIO: <input type='text' name='user' value=''/> <br/>";
			echo "TELEFONO: <input type='text' name='tf' value=''/> <br/>";
			echo "EMAIL: <input type='text' name='email' value=''/> <br/>";
			echo "<input type='submit' value='Añadir'/>";
			echo "<input type='hidden' name='pagina' value='add'/>";
			echo "<input type='reset' value='Cancelar'/>";
			echo "</form>";
			echo "<hr/>";
			echo "</td>";
			echo "</tr>";
		}
		else{
			$_SESSION["aGuardar"] =  $_POST["user"].','.$_POST["tf"].','.$_POST["email"]; //controlamos los parametros post
			header("Location: index1.php");
			return;
		}
}	
elseif (isset($_POST["pagina"]) && $_POST["pagina"] == 'delete'){
		$mensaje = "";
		$datos = leerDatosFichero("txt/agenda.txt");
		$id = $_POST["idEliminar"];

		if(strlen($id) == 0){
			$mensaje = "Introduzca un numero si desea eliminar un elemento de la agenda";
			echo "<tr align='center'>";
			echo "<td>";
			echo "<h2> Borrar </h2>";
			echo "<table border=1px>";
			echo "<tr><td>ID</td><td>USUARIO</td><td>TELEFONO</td><td>EMAIL</td></tr>";
				for($i = 0; $i< count($datos);$i++){
					$array = explode(",", $datos[$i]);
					echo "<tr><td>$i</td><td>".$array[0]."</td><td>".$array[1]."</td><td>".$array[2]."</td></tr>";
				}
			echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "<tr align='center'><td style='color:red'> $mensaje </td></tr>";
			echo "<tr align='center'><td><form method='post'> Introduce el ID del elemento a eliminar: <input type='text' name='idEliminar' value=''/></td>";
			echo "<tr align='center'><td><input type='submit' value='Borrar'/>";
			echo "<input type='hidden' name='pagina' value='delete'/>";
			echo "<input type='reset' value='Cancelar'/><hr/></form>";
			echo "</td></tr>";
		}
		elseif(intval($id)< 0 || intval($id) >= count($datos)){
				$mensaje = "ID no registrado. Compruebe que exista como ID en la agenda";
				echo "<tr align='center'>";
			echo "<td>";
			echo "<h2> Borrar </h2>";
			echo "<table border=1px>";
			echo "<tr><td>ID</td><td>USUARIO</td><td>TELEFONO</td><td>EMAIL</td></tr>";
				for($i = 0; $i< count($datos);$i++){
					$array = explode(",", $datos[$i]);
					echo "<tr><td>$i</td><td>".$array[0]."</td><td>".$array[1]."</td><td>".$array[2]."</td></tr>";
				}
			echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "<tr align='center'><td style='color:red'> $mensaje </td></tr>";
			echo "<tr align='center'><td><form method='post'> Introduce el ID del elemento a eliminar: <input type='text' name='idEliminar' value=''/></td>";
			echo "<tr align='center'><td><input type='submit' value='Borrar'/>";
			echo "<input type='hidden' name='pagina' value='delete'/>";
			echo "<input type='reset' value='Cancelar'/><hr/></form>";
			echo "</td></tr>";
		}
		else{
			$_SESSION["aBorrar"] = $id;
			header("Location: index1.php");
			return;
		}
}
else{
	echo "<tr align='center'>";
	echo "<td>";
		echo "<h2> Bienvenido ".$_SESSION["user"]."</h2>";
		echo "<h2> Seleccione una opción </h2>";
	echo "</td>";
echo "</tr>";
}	
?>
<?php
echo "<tr align='center'>";
	echo "<td>";
		echo "<form method='post' action='index1.php'>";
			echo "<input type='submit' name ='Inicio' value='Inicio'/>";
			echo "<input type='submit' name='pagina' value='Listar'/>";
			echo "<input type='submit' name='pagina' value='Añadir'/>";
			echo "<input type='submit' name='pagina' value='Borrar'/>";
			echo "<input type='submit' name='pagina' value='Salir'/>";
		echo "</form>";
	echo "</td>";
echo "</tr>";
?>
<tr>
	<td> <?php include('pie.php'); ?> </td>
</tr>
</table>
</body>
</html>
